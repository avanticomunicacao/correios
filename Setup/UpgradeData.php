<?php

namespace ImaginationMedia\Correios\Setup;

use Magento\Catalog\Model\Product as Product;
use Magento\Eav\Setup\EavSetup;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;

class UpgradeData implements UpgradeDataInterface
{
    /**
     * @var EavSetup
     */
    private EavSetup $eavSetup;

    /**
     * UpgradeData constructor.
     *
     * @param EavSetup $eavSetup
     */
    public function __construct(EavSetup $eavSetup)
    {
        $this->eavSetup = $eavSetup;
    }

    /**
     *
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function upgrade(
        ModuleDataSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '1.3.4', '<=')) {
            $this->eavSetup->updateAttribute(Product::ENTITY, 'correios_width', 'is_filterable', false);
            $this->eavSetup->updateAttribute(Product::ENTITY, 'correios_height', 'is_filterable', false);
            $this->eavSetup->updateAttribute(Product::ENTITY, 'correios_depth', 'is_filterable', false);
        }

        $setup->endSetup();
    }
}
